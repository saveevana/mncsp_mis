
/************Email Validation****************/
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/**********Organization Update***********/
function organization_edit(org_id) {
    //alert(dept_id);
    var postData =
        {
            "organization_id": org_id
        };

    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: base_url + 'ajax/organization_update',
        success: function (msg) {
            //alert(msg);
            $("#organization_edit").html(msg);
        }
    });
    return false;
}


/**************organization Delete***************/
function organization_delete(org_id) {
    var postData =
        {
            "organization_id": org_id
        };
    var dataString = JSON.stringify(postData);
    sci_confirm("Are you sure to delete this organization?", "Delete", function (res) {
        if (res)
        //sci_alert('message will go here');
        {
            $.ajax({
                type: "POST",
                data: {jsondata: dataString},
                url: base_url + 'organization/organization_delete',
                success: function (msg) {
                    //alert(msg);
                    sci_alert("", msg, "success");
                    // $(".col-sm-12").load(" .col-sm-12");
                    $("#example1").load(" #example1");
                }
            });
        }
    });
    return false;
}


function organization_create()
{
  //alert("hello");

  var org_name = $(this).attr('organization_name').value;
  var org_location = $(this).attr('organization_location').value;
  //alert(organization_name);
  //alert(organization_location);

  if(org_name=='')
     {

     alert('Please Enter Organization Name');
     document.getElementById("organization_name").focus();
     document.getElementById("organization_name").style.background = "#e2eef5";
     return false;
     }

  var postData =
        {
            "organization_name": org_name,
            "organization_location": org_location,
        };

  var dataString = JSON.stringify(postData);

  $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: base_url+'ajax/organization_create',
        success: function (msg) {
            alert(msg);
            //sci_alert("", msg, "success");
            $("#organization_form").load(" #organization_form");
            
        }
    });


  return false;  
}

/*********  Employee Create *************/
function employee_create()
{
  var employee_name = $(this).attr('employee_name').value;
  var designation = $(this).attr('designation').value;
  var category = $(this).attr('category').value;
  var thematic_team = $(this).attr('thematic_team').value;
  var organization = $(this).attr('organization').value;
  var organization_location = $(this).attr('organization_location').value;
  var mobile_no = $(this).attr('mobile_no').value;
  var email = $(this).attr('email').value;
  var org_id = $(this).attr('org_id').value;
  var status = 1;
  var supervisor = $(this).attr('supervisor').value;

  //alert(organization_name);
  //alert(organization_location);

  if(employee_name=='')
     {

     alert('Please Enter Employee Name');
     document.getElementById("employee_name").focus();
     document.getElementById("employee_name").style.background = "#e2eef5";
     return false;
     }

    if(designation=='')
     {

     alert('Please Select Designation');
     document.getElementById("designation").focus();
     document.getElementById("designation").style.background = "#e2eef5";
     return false;
     }

     if(category=='')
     {

     alert('Please Select Category');
     document.getElementById("category").focus();
     document.getElementById("category").style.background = "#e2eef5";
     return false;
     }

     if(organization=='')
     {

     alert('Please Select Organization');
     document.getElementById("organization").focus();
     document.getElementById("organization").style.background = "#e2eef5";
     return false;
     }

    if(mobile_no=='')
     {
     alert('Please Enter Mobile Number');
     document.getElementById("mobile_no").focus();
     document.getElementById("mobile_no").style.background = "#e2eef5";
     return false;
     }

     if(email=='')
     {

     alert('Please Enter Email ID');
     document.getElementById("email").focus();
     document.getElementById("email").style.background = "#e2eef5";
     return false;
     }

      if (validateEmail(email)) 
      {
        //return 1;
        var email = $(this).attr('email').value;
        }
        else {
            alert('Please Enter Valid Email Address.');
            document.getElementById("email").focus();
            document.getElementById("email").style.background = "#e2eef5";
            return false;
        }
     if(org_id=='')
     {
     alert('Please Enter Organization ID');
     document.getElementById("org_id").focus();
     document.getElementById("org_id").style.background = "#e2eef5";
     return false;
     }
     if(supervisor=='')
     {

     alert('Please Select Supervisor');
     document.getElementById("supervisor").focus();
     document.getElementById("supervisor").style.background = "#e2eef5";
     return false;
     }

     
  var postData =
        {
            "employee_name": employee_name,
            "designation": designation,
            "org_id": org_id,
            "email": email,
            "organization": organization,
            "location": organization_location,
            "themetic_team": thematic_team,
            "category": category,
            "mobile": mobile_no,
            "supervisor": supervisor,
            "status": 1,
        };

  var dataString = JSON.stringify(postData);

  $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: base_url+'ajax/employee_create',
        success: function (msg) {
            alert(msg);
            //sci_alert("", msg, "success");
            $("#employee_form").load(" #employee_form");
            
        }
    });


  return false;  

}

/**********Employee Update***********/
function employee_edit(emp_id) {
    //alert(dept_id);
    var postData =
        {
            "employee_id": emp_id,
        };

    var dataString = JSON.stringify(postData);
    $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: base_url + 'ajax/employee_update',
        success: function (msg) {
            //alert(msg);
            $("#employee_edit").html(msg);
        }
    });
    return false;
}

/******* employee update submit**********/
function employee_update_submit()
{
  
  var employee_id = $(this).attr('employee_id').value;
  var employee_name = $(this).attr('employee_name').value;
  var designation = $(this).attr('designation').value;
  var category = $(this).attr('category').value;
  var thematic_team = $(this).attr('thematic_team').value;
  var organization = $(this).attr('organization').value;
  var organization_location = $(this).attr('organization_location').value;
  var mobile_no = $(this).attr('mobile_no').value;
  var email = $(this).attr('email').value;
  var org_id = $(this).attr('org_id').value;
  var status = 1;
  var supervisor = $(this).attr('supervisor').value;

  //alert(organization_name);
  //alert(organization_location);

  if(employee_name=='')
     {

     alert('Please Enter Employee Name');
     document.getElementById("employee_name").focus();
     document.getElementById("employee_name").style.background = "#e2eef5";
     return false;
     }

    if(designation=='')
     {

     alert('Please Select Designation');
     document.getElementById("designation").focus();
     document.getElementById("designation").style.background = "#e2eef5";
     return false;
     }

     if(category=='')
     {

     alert('Please Select Category');
     document.getElementById("category").focus();
     document.getElementById("category").style.background = "#e2eef5";
     return false;
     }

     if(organization=='')
     {

     alert('Please Select Organization');
     document.getElementById("organization").focus();
     document.getElementById("organization").style.background = "#e2eef5";
     return false;
     }

    if(mobile_no=='')
     {
     alert('Please Enter Mobile Number');
     document.getElementById("mobile_no").focus();
     document.getElementById("mobile_no").style.background = "#e2eef5";
     return false;
     }

     if(email=='')
     {

     alert('Please Enter Email ID');
     document.getElementById("email").focus();
     document.getElementById("email").style.background = "#e2eef5";
     return false;
     }

      if (validateEmail(email)) 
      {
        //return 1;
        var email = $(this).attr('email').value;
        }
        else {
            alert('Please Enter Valid Email Address.');
            document.getElementById("email").focus();
            document.getElementById("email").style.background = "#e2eef5";
            return false;
        }
     if(org_id=='')
     {
     alert('Please Enter Organization ID');
     document.getElementById("org_id").focus();
     document.getElementById("org_id").style.background = "#e2eef5";
     return false;
     }
     if(supervisor=='')
     {

     alert('Please Select Supervisor');
     document.getElementById("supervisor").focus();
     document.getElementById("supervisor").style.background = "#e2eef5";
     return false;
     }

     
  var postData =
        {
            "employee_id": employee_id,
            "employee_name": employee_name,
            "designation": designation,
            "org_id": org_id,
            "email": email,
            "organization": organization,
            "location": organization_location,
            "themetic_team": thematic_team,
            "category": category,
            "mobile": mobile_no,
            "supervisor": supervisor,
            "status": 1,
        };

  var dataString = JSON.stringify(postData);

  $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: base_url+'employee/employee_update',
        success: function (msg) {
            alert(msg);

        }
    });


  return false;   
}

function employee_delete(emp_id)
{

     if (confirm("Are you sure you want to delete this employee?")) 
     {
    
         $.ajax({
                type: "POST",
                data: {emp_id: emp_id},
                url: base_url +'employee/employee_delete',
                success: function (msg) {
                    alert(msg);
                    
                    $("#dataTables-example").load(" #dataTables-example");
                }
            });
    } else {
        return false;
    } 
}

/*********  Designation create form *************/

function designation_create()
    {
        var designation = $(this).attr('designation').value;
 

  if(designation=='')
     {

     alert('Please Enter Designation');
     document.getElementById("designation").focus();
     document.getElementById("designation").style.background = "#e2eef5";
     return false;
     }

  var postData =
        {
            "designation": designation,
            "status": 1,
           
        };

  var dataString = JSON.stringify(postData);

  $.ajax({
        type: "POST",
        data: {jsondata: dataString},
        url: base_url+'ajax/designation_create',
        success: function (msg) {
            alert(msg);
            //sci_alert("", msg, "success");
            $("#designation_form").load(" #designation_form");
            
        }
    });


  return false;  

}

/**********Designation Update***********/
function designation_edit(designation_id) 
{
    
    alert(designation_id);
    
    return false;
}
 