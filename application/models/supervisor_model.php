<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: supervisor_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP
// @license An open source application
// @Version     1.0
// Function list: organization_list,
/****************************************************/

/**
 * AMS organization model class
 *
 * This method demonstrates the organization data of AMS.
 */
class supervisor_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    //****** Get Supervisor List ************/
    function supervisor_list()
    {
        $this->db->order_by('category', 'asc'); // or 'DESC'
        $this->db->select('*');
        $this->db->from('adm_category');
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}

?>
