<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: organization_model.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP
// @license An open source application
// @Version     1.0
// Function list: organization_list,
/****************************************************/

/**
 * AMS organization model class
 *
 * This method demonstrates the organization data of AMS.
 */
class employee_model extends CI_Model
{

    function __construct()
    {
        parent:: __construct();
        $this->load->database();
    }

    //****** Get Employee List ************/
    function employee_list()
    {
        
        $this->db->select('a.*,b.*,c.*,d.designation_id,d.designation as desig_name');
        $this->db->from('adm_employee a, adm_category b, adm_organization c, adm_designation d');
        $this->db->where('a.designation = d.designation_id');
        $this->db->where('a.organization = c.org_id');
        $this->db->where('a.category = b.category_id');
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /****   Employee Delete*****/
    function employee_delete($emp_id)
    {
        $this->db->where('employee_id', $emp_id);
        $query=$this->db->delete('adm_employee');
        
        return $query;
    }
}

?>
