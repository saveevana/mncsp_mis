<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: organization.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP)
// @license An open source application
// @Version     1.0
// Function list: 
/****************************************************/

 
class organization extends ADMIN_Controller
{

    function __construct()
    {
        parent::__construct();


        $this->load->model('organization_model', '', TRUE);   

        $this->load->library('encrypt');
        $this->load->library('session');
        
        $this->load->helper('form');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Organization');
        $this->set_page_sub_title('control panel');
    }

    
    /** 
    * This function generates the organization creation form.
    */
    public function new_organization()
    {
        $this->set_page_title(load_message('ADD_NEW_ORGANIZATION'));
        $this->set_page_sub_title('');
       
        $this->set_js('dist/js/setting_script.js');
        $this->load_view('admin/organization/organization_create');
    }


    /** 
    * This function retrieves the userlist from database.
    */
    public function organization_list()
    {
        $this->set_page_title(load_message('ORGANIZATION_LIST'));
        $this->set_page_sub_title('');
        
        $result = $this->organization_model->organization_list();
        //var_dump($result);
        $this->set_value('organization_list', $result);
        $this->load_view('admin/organization/organization_list');
    }

}
