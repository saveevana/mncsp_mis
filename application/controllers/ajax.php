<?php
/****************************************************/
// Filename: ajax.php
// Created By:     Evana Yasmin 18 Sept 2018
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP
// @license An open source application
// @Version     1.0
// Function list: 
//
/****************************************************/

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax extends CI_Controller
{


    function __construct()
    {
        parent::__construct();


        $this->load->library('encrypt');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('user_model', '', TRUE);
       //$this->lang->load('message','english');
    }

    
    /** 
    *This function demostrates the user organization process of MIS. 
    */
    public function organization_create()
    {
        
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request())
             {
                //echo"Got it!";
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);

                 
                    $res = sci_insert_db('organization', $jsondata);
                        if ($res <> false) 
                        {
                            echo "Organization created successfully!";
                        }
                        else
                            {
                                echo"Sorry! Failed to insert data.";
                            }
                    
                }
             else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

 /***********  Employee Create*************/ 
 public function employee_create()
 {

    if ($this->session->userdata('user_logged_in')) {
        if ($this->input->is_ajax_request())
             {
                //echo"Got it!";
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                    $res = sci_insert_db('employee', $jsondata);
                    if ($res <> false) {

                        echo "Employee registered successfully!";
                    }
                    else
                        {
                            echo"Sorry! Failed to insert data.";
                        }
                    
                }
             else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }

 }

 /**********  Employee Update *************/

public function employee_update()
    {

        $this->load->model('employee_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);   
        $this->load->model('designation_model', '', TRUE);
        $this->load->model('organization_model', '', TRUE); 
            
        if ($this->session->userdata('user_logged_in')) 
        {
           
            if ($this->input->is_ajax_request()) 
            {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('employee', ['employee_id' => $jsondata->employee_id]);

                $data['employee_details'] = $result;

                $result1 = $this->organization_model->organization_list();
                $result2 = $this->designation_model->designation_list();
                $result3 = $this->category_model->category_list();
                $result4 = $this->employee_model->employee_list();

                $data['organization_list'] = $result1;
                $data['designation_list'] = $result2;
                $data['category_list'] = $result3;
                $data['supervisor_list'] = $result4;

                $this->load->view('admin/employee/employee_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }
  

  /*** designation create****/
  public function designation_create()
  {
    if ($this->session->userdata('user_logged_in')) {
        if ($this->input->is_ajax_request())
             {
                //echo"Got it!";
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                    $res = sci_insert_db('designation', $jsondata);
                    if ($res <> false) {

                        echo "Designation created successfully!";
                    }
                    else
                        {
                            echo"Sorry! Failed to insert data.";
                        }
                    
                }
             else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
  }

/* 
* This function retrieves the detail information of existing organization for update purpose.
*/ 
public function organization_update()
    {

        if ($this->session->userdata('user_logged_in')) {

           
            if ($this->input->is_ajax_request()) {
                $jsondata = json_decode($this->input->post('jsondata', true));
                $result = sci_select_db('organization', ['org_id' => $jsondata->organization_id]);

                $data['organization_details'] = $result;
                $this->load->view('admin/organization/organization_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }


/*** Category create****/
  public function category_create()
  {
    if ($this->session->userdata('user_logged_in')) {
        if ($this->input->is_ajax_request())
             {
                //echo"Got it!";
                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                    $res = sci_insert_db('category', $jsondata);
                    if ($res <> false) {

                        echo "Category created successfully!";
                    }
                    else
                        {
                            echo"Sorry! Failed to insert data.";
                        }
                    
                }
             else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
  }


/** 
* This function inserts new user role process of AMS. 
*/
    public function userrole_submit()
    {
        $this->load->model('role_model', '', TRUE);
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {

                $role_name = $this->input->post('role_name', true);
                $role_rank = $this->input->post('role_rank', true);
                $check_string = $this->input->post('check_string', true);

                $len = strlen($check_string);
                $check_string[$len - 1] = '';
                $privilege = trim($check_string);
                //echo $check_string;
                $result = $this->role_model->role_create($role_name,$role_rank, $privilege);
                //echo"Successfully inserted";
                if($result <> false)
                 {
                   $action_name = "User Role Create"; //** log file create**/
                   $log_json["role_name"] = $role_name;
                   $log_json["role_rank"] = $role_rank;
                   $log_json["privilege"] = $privilege;

                   log_create($action_name,json_encode($log_json,true));
                   echo $this->lang->line('INSERT');
                 }   
               
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function inserts new privilege of AMS. 
    */
    public function privilege_submit()
    {

        if ($this->session->userdata('user_logged_in')) {

            if ($this->input->is_ajax_request()) {

                $jsondata = $this->input->post('jsondata', true);
                //var_dump($jsondata);
                $result = sci_insert_db('privilege', $jsondata);
                if ($result <> false) {

                   $action_name = "Role Privilege Create"; //** log file create **/
                   log_create($action_name,$jsondata);

                echo '{"status": "success", "message": "' . $this->lang->line('PRIVILEGE_CREATED') . '"}';
                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('PRIVILEGE_CREATE_FAILED') . '"}';
                }

            } else {
                exit('No direct script access allowed.');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the privilege details from privilege_id. 
    */
    public function privilege_details()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('privilege_model', '', TRUE);
                $privilege_id = $this->input->post('privilege_id', true);
                //$jsondata =  json_decode($this->input->post('jsondata',true),true);

                //var_dump($jsondata);
                $result = $this->privilege_model->privilege_details($privilege_id);
                if ($result <> false) {
                    $data['privilege_name'] = $result;
                }
                $this->load->view('admin_lte/privilege/privilege_name', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the privilege details from privilege_id. 
    */
    public function privilege_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('privilege_model', '', TRUE);
                $privilege_id = $this->input->post('privilege_id', true);

                $result = $this->privilege_model->privilege_details($privilege_id);

                //$this->set_value('privilege_name',$result);
                $data['privilege_details'] = $result;
                $this->load->view('admin_lte/privilege/privilege_update', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the profile details from userid. 
    */
    public function profile_details()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('user_model', '', TRUE);
                $db_userid = $this->input->post('userid', true);

                $result = $this->user_model->user_details($db_userid);

                //var_dump($result);
                $data['profile_details'] = $result;
                $this->load->view('admin_lte/user/user_details', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function retrieves the profile details from userid. 
    */
    public function profile_update()
    {

        //$this->set_js('dist/js/office.js');

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {

                $this->load->model('user_model', '', TRUE);
                $this->load->model('role_model', '', TRUE);
                $this->load->model('office_model', '', TRUE);
                $this->load->model('department_model', '', TRUE);
                $dept_list = $this->department_model->department_list();

                $db_userid = $this->input->post('userid', true);
                $result = $this->user_model->user_details($db_userid);

                //var_dump($result);
                $result1 = $this->role_model->rolelists();
                $result2 = $this->office_model->officelist();

                $data['profile_details'] = $result;
                $data['role_list'] = $result1;
                $data['office_list'] = $result2;
                $data['dept_list']=$dept_list;

                $this->load->view('admin_lte/user/user_update', $data);

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the role details. 
    */
    public function role_update()
    {

        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('role_model', '', TRUE);
                $role_id = $this->input->post('role_id', true);

                $result = $this->role_model->role_details($role_id);
                //var_dump($result);
                $data['role_details'] = $result;
                $rolelist = $this->role_model->rolelist();
                $data['role_list'] = $rolelist;
                /*********All Privilege List***********/
                $result1 = $this->role_model->privilegelist();
                $data['privilege_list'] = $result1;
                $this->load->view('admin_lte/role/role_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function completes the category creation process. 
    */
    public function category_submit()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $jsondata = $this->input->post('jsondata', true);
                $jsondata = json_decode($jsondata, true);
                if($jsondata['parent_id']==0){
                    $jsondata['parent_id']= null;
                }
                if($jsondata['lifetime']==0){
                    $jsondata['lifetime']= null;
                }
                $jsondata = json_encode($jsondata, true);
                $result = sci_insert_db('category', $jsondata);
                if ($result <> false) {

                   $action_name = "Category Create"; //** log file create**/
                   log_create($action_name,$jsondata);

                    echo '{"status": "success", "message": "' . $this->lang->line('CATEGORY_CREATED') . '"}';

                } else {
                    echo '{"status": "error", "message": "' . $this->lang->line('CATEGORY_CREATE_FAILED') . '"}';

                }

            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

    /** 
    * This function updates the category details. 
    */
    public function category_update()
    {
        if ($this->session->userdata('user_logged_in')) {
            if ($this->input->is_ajax_request()) {
                $this->load->model('category_model', '', TRUE);
                $category_id = $this->input->post('category_id', true);
                $result = $this->category_model->category_details($category_id);
                //var_dump($result);
                $data['category_details'] = $result;

                /*********All Privilege List***********/
                $result1 = $this->category_model->categorylist();
                $data['category_list'] = $result1;

                $this->load->view('admin_lte/category/category_update', $data);
            } else {
                exit('No direct script access allowed');
            }
        } else {
            redirect('login');
        }
    }

}