<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: organization.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP)
// @license An open source application
// @Version     1.0
// Function list: 
/****************************************************/

 
class category extends ADMIN_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('category_model', '', TRUE);
       
        $this->load->library('encrypt');
        $this->load->library('session');
        
        $this->load->helper('form');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Category');
        $this->set_page_sub_title('control panel');
    }

    
    /** 
    * This function generates the category creation form.
    */
    public function new_category()
    {
        $this->set_page_title(load_message('ADD_NEW_CATEGORY'));
        $this->set_page_sub_title('');
       
        $this->set_js('dist/js/setting_script.js');
        $this->load_view('admin/category/category_create');
    }


    /** 
    * This function retrieves the category records from database.
    */
    public function category_list()
    {
        $this->set_page_title(load_message('CATEGORY_LIST'));
        $this->set_page_sub_title('');
        
        $result = $this->category_model->category_list();
        //var_dump($result);
        $this->set_value('category_list', $result);
        $this->load_view('admin/category/category_list');
    }

    /**   Category Update submit******/

    public function category_update()
    {
         if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true), true);
            $category_id = $jsondata["category_id"];
            unset($jsondata["category_id"]);
            $result = sci_update_db('category', $jsondata, ['category_id' => $category_id]);
            if ($result <> false) {
                echo"Category is updated!";
            }


        } else {
            exit('No direct script access allowed');
        }
    }

    /**********  Category Delete **********/
    public function category_delete()
    {
       if ($this->input->is_ajax_request()) {
            $designation_id = $this->input->post('designation_id', true);

            $result = $this->designation_model->designation_delete($designation_id);

            if ($result <> false) {
                echo"Designation is deleted!";
            }


        } else {
            exit('No direct script access allowed');
        } 
    }

}
