<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends ADMIN_Controller {

    public function index() {
        $this->set_page_title("Dashboard");
        $this->load_view('admin/blocks/dashboard');
    }

}
