<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: organization.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP)
// @license An open source application
// @Version     1.0
// Function list: 
/****************************************************/

 
class employee extends ADMIN_Controller
{

    function __construct()
    {
        parent::__construct();


        $this->load->model('employee_model', '', TRUE);
        $this->load->model('category_model', '', TRUE);   
        $this->load->model('designation_model', '', TRUE);
        $this->load->model('organization_model', '', TRUE);   

        $this->load->library('encrypt');
        $this->load->library('session');
        
        $this->load->helper('form');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Employee');
        $this->set_page_sub_title('control panel');
    }

    
    /** 
    * This function generates the employee creation form.
    */
    public function register()
    {
        $this->set_page_title(load_message('ADD_NEW_EMPLOYEE'));
        $this->set_page_sub_title('');
       
        $result = $this->organization_model->organization_list();
        $result1 = $this->designation_model->designation_list();
        $result2 = $this->category_model->category_list();
        $result3 = $this->employee_model->employee_list();

        $this->set_value('organization_list', $result);
        $this->set_value('designation_list', $result1);
        $this->set_value('category_list', $result2);
        $this->set_value('supervisor_list', $result3);


        $this->set_js('dist/js/setting_script.js');
        $this->load_view('admin/employee/register');
    }


    /** 
    * This function retrieves the employee records from database.
    */
    public function employee_list()
    {
        $this->set_page_title(load_message('EMPLOYEE_LIST'));
        $this->set_page_sub_title('');
        
        $result = $this->employee_model->employee_list();
        //var_dump($result);
        $this->set_value('employee_list', $result);
        $this->load_view('admin/employee/employee_list');
    }

    /**   Employee Update submit******/

    public function employee_update()
    {
         if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true), true);
            $employee_id = $jsondata["employee_id"];
            unset($jsondata["employee_id"]);
            $result = sci_update_db('employee', $jsondata, ['employee_id' => $employee_id]);
            if ($result <> false) {
                echo"Employee Information is updated!";
            }


        } else {
            exit('No direct script access allowed');
        }
    }

    /**********  Employee Delete**********/
    public function employee_delete()
    {
       if ($this->input->is_ajax_request()) {
            $emp_id = $this->input->post('emp_id', true);

            $result = $this->employee_model->employee_delete($emp_id);

            if ($result <> false) {
                echo"Employee is deleted!";
            }


        } else {
            exit('No direct script access allowed');
        } 
    }

}
