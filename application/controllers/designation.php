<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/****************************************************/
// Filename: organization.php
// Created By:     Evana Yasmin 
// Change history:
//      
// @copyright   Copyright (c) 2018 - 2019, SCI.
// @copyright   Copyright (c) 2018 - 2019, Mamoni-MNCSP)
// @license An open source application
// @Version     1.0
// Function list: 
/****************************************************/

 
class designation extends ADMIN_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('designation_model', '', TRUE);
       
        $this->load->library('encrypt');
        $this->load->library('session');
        
        $this->load->helper('form');
        $this->load->helper('url');

    }

    public function index()
    {
        $this->set_page_title('Designation');
        $this->set_page_sub_title('control panel');
    }

    
    /** 
    * This function generates the designation creation form.
    */
    public function create_designation()
    {
        $this->set_page_title(load_message('ADD_NEW_DESIGNATION'));
        $this->set_page_sub_title('');
       
        $this->set_js('dist/js/setting_script.js');
        $this->load_view('admin/designation/designation_create');
    }


    /** 
    * This function retrieves the employee records from database.
    */
    public function designation_list()
    {
        $this->set_page_title(load_message('EMPLOYEE_LIST'));
        $this->set_page_sub_title('');
        
        $result = $this->designation_model->designation_list();
        //var_dump($result);
        $this->set_value('designation_list', $result);
        $this->load_view('admin/designation/designation_list');
    }

    /**   Employee Update submit******/

    public function designation_update()
    {
         if ($this->input->is_ajax_request()) {
            $jsondata = json_decode($this->input->post('jsondata', true), true);
            $employee_id = $jsondata["designation_id"];
            unset($jsondata["designation_id"]);
            $result = sci_update_db('designation', $jsondata, ['designation_id' => $designation_id]);
            if ($result <> false) {
                echo"Designation is updated!";
            }


        } else {
            exit('No direct script access allowed');
        }
    }

    /**********  Designation Delete **********/
    public function designation_delete()
    {
       if ($this->input->is_ajax_request()) {
            $designation_id = $this->input->post('designation_id', true);

            $result = $this->designation_model->designation_delete($designation_id);

            if ($result <> false) {
                echo"Designation is deleted!";
            }


        } else {
            exit('No direct script access allowed');
        } 
    }

}
