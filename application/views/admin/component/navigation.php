<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Mamoni-MNCSP: MIS</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <?php $this->load->view('admin/component/msg_notification');?>
        <?php $this->load->view('admin/component/task_notification');?>
        <?php $this->load->view('admin/component/alert_notification');?>
        
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?= base_url();?>user/profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="<?= base_url();?>user/settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?= base_url(); ?>login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="<?= base_url();?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
              
                <li>
                    <a href="#"><i class="fa fa-user fa-fw"></i> Employee<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?= base_url();?>employee/employee_list"><i class="fa fa-angle-right"></i> Employee List</a>
                        </li>
                        <li>
                            <a href="<?= base_url();?>employee/register"><i class="fa fa-angle-right"></i> Register Employee</a>
                        </li>
                       
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Report<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="flot.html">Flot Charts</a>
                        </li>
                        <li>
                            <a href="morris.html">Morris.js Charts</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
               </li>
                <li>
                    <a href="#"><i class="fa fa-cogs"></i> Settings <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        
                        <li>
                            <a href="#">Designation<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?= base_url();?>designation/designation_list"><i class="fa fa-angle-right"></i> Designation List</a>
                                </li>
                                <li>
                                    <a href="<?= base_url();?>designation/create_designation"><i class="fa fa-angle-right"></i> Create Designation</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                         <li>
                            <a href="#">Organization<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?= base_url();?>organization/organization_list"><i class="fa fa-angle-right"></i> Organization List</a>
                                </li>
                                <li>
                                    <a href="<?= base_url();?>organization/new_organization"><i class="fa fa-angle-right"></i> Create Organization</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                        <li>
                            <a href="#">Category<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?= base_url();?>category/category_list"><i class="fa fa-angle-right"></i> Category List</a>
                                </li>
                                <li>
                                    <a href="<?= base_url();?>category/new_category"><i class="fa fa-angle-right"></i> Create Category</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                        <li>
                            <a href="#">Supervisor Assign<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?= base_url();?>supervisor/assign_supervisor"><i class="fa fa-angle-right"></i> Assign Supervisor</a>
                                </li>        
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                         <li>
                            <a href="#">User Role<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?= base_url();?>role/role_list"><i class="fa fa-angle-right"></i> Role List</a>
                                </li>
                                <li>
                                    <a href="<?= base_url();?>role/create_role"><i class="fa fa-angle-right"></i> Create Role</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                        <li>
                            <a href="#">Role Privilege<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="<?= base_url();?>privilege/designation_list"><i class="fa fa-angle-right"></i> Privilege List</a>
                                </li>
                                <li>
                                    <a href="<?= base_url();?>privilege/create_designation"><i class="fa fa-angle-right"></i> Create Privilege</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
    
</nav>