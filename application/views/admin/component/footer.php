</div>
<!-- /#wrapper -->   
    <!-- jQuery -->
    <?php load_js("vendor/jquery/jquery.min.js"); ?>
    
    <!-- Bootstrap Core JavaScript -->
    <?php load_js("vendor/bootstrap/js/bootstrap.min.js"); ?>
    
    <!-- Metis Menu Plugin JavaScript -->
    <?php load_js("vendor/metisMenu/metisMenu.min.js"); ?>
	
	 <?php load_js("vendor/datatables/js/jquery.dataTables.min.js"); ?>
   
    <?php load_js("vendor/datatables-plugins/dataTables.bootstrap.min.js"); ?>
    
    <?php load_js("vendor/datatables-responsive/dataTables.responsive.js"); ?>
	   
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    
    <!-- Morris Charts JavaScript -->
    <?php load_js("vendor/raphael/raphael.min.js"); ?>
    <?php load_js("vendor/morrisjs/morris.min.js"); ?>
    <?php load_js("data/morris-data.js"); ?>

    <!-- Custom Theme JavaScript -->
    <?php load_js("dist/js/sb-admin-2.js"); ?>
    <?php load_js("dist/js/setting_script.js"); ?>
    <?php //load_js("dist/js/custom_msg.js"); ?>
    <?php //load_js("dist/js/sci_export.js"); ?>


</body>

</html>