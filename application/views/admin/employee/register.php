			
				<div class="row">
				<div class="col-lg-10">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                           Employee Registration Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                   <form role="form" id="employee_form">
                                        <div class="form-group">
                                          <label>Name of Employee <span class="text-danger">*</span></label>
                                          <input class="form-control" id="employee_name" name="employee_name" placeholder="Enter Employee Name">
                                            
                                        </div>
                                        
                                         <div class="form-group">
                                          <label>Designation<span class="text-danger">*</span></label>
                                          <select name="designation" id="designation" class="form-control">
                                          <option value="" selected>Select Designation</option>
                                          <?php 
										  if($designation_list<>"")
										  {
										  foreach($designation_list as $dglist)
										  	{
										  		$designation_id = $dglist->designation_id;
												$designation = $dglist->designation;
												?>
                                                 <option value="<?php echo $designation_id;?>"><?php echo $designation;?></option>
                                                <?php 
										  	}
										  }
										  ?>
                                          </select>      
                                        </div>
                                        
                                         <div class="form-group">
                                          <label>Category<span class="text-danger">*</span></label>
                                          <select name="category" id="category" class="form-control">
                                         
                                          <?php 
										  if($category_list<>"")
										  {
										  foreach($category_list as $cglist)
										  	{
										  		$category_id = $cglist->category_id;
												$category = $cglist->category;
												?>
                                                 <option value="<?php echo $category_id;?>"><?php echo $category;?></option>
                                                <?php 
										  	}
										  }
										  ?>
                                          </select>     
                                        </div>
                                        
                                         <div class="form-group">
                                        <label>Thematic Team</label>
                                        <input class="form-control" id="thematic_team" name="thematic_team" placeholder="Enter Thematic Team">
                                        </div>
                                        
                                        <div class="form-group">
                                          <label>Organization<span class="text-danger">*</span></label>
                                          <select name="organization" id="organization" class="form-control">
                                          <option value="" selected>Select Organization</option>
                                          <?php 
										  if($organization_list<>"")
										  {
										  foreach($organization_list as $oglist)
										  	{
										  		$organization_id = $oglist->org_id;
												$organization_name = $oglist->organization_name;
												?>
                                                 <option value="<?php echo $organization_id;?>"><?php echo $organization_name;?></option>
                                                <?php 
										  	}
										  }
										  ?>
                                          </select>     
                                        </div>
                                        
                                        <div class="form-group">
                                        <label>Location</label>
                                        <input class="form-control" id="organization_location" name="organization_location" 
                                        placeholder="Enter Organization Location">
                                        </div>
                                        
                                        <div class="form-group">
                                        <label>Mobile No.</label>
                                        <input class="form-control" id="mobile_no" name="mobile_no" placeholder="Enter Mobile Number">
                                        </div>
                                        
                                        <div class="form-group">
                                        <label>Email ID<span class="text-danger">*</span></label>
                                        <input class="form-control" id="email" name="email" placeholder="Enter Email ID">
                                        </div>
                                        
                                        <div class="form-group">
                                        <label>Organization ID<span class="text-danger">*</span></label>
                                        <input class="form-control" id="org_id" name="org_id" placeholder="Enter Organization ID">
                                        </div>
                                        
                                       
                                        <div class="form-group">
                                        <label>Supervisor<span class="text-danger">*</span></label>
                                        <select class="form-control" name="supervisor" id="supervisor">
                                        <option value="" selected>Select Supervisor</option>
                                         <?php 
										  if($supervisor_list<>"")
										  {
										  foreach($supervisor_list as $splist)
										  	{
										  		$employee_id = $splist->employee_id;
												$employee_name = $splist->employee_name;
												?>
                                                 <option value="<?php echo $employee_id;?>"><?php echo $employee_name;?></option>
                                                <?php 
										  	}
										  }
										  ?>
                                        </select>
                                        </div>
                                       
                                        <div align="right">
                                        <button type="button" class="btn btn-outline btn-default" onClick="return employee_create();">
                                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('SUBMIT'); ?>
                                        </button>
                                        </div>
                                    </form>
                                </div>
                              
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
				</div>

   