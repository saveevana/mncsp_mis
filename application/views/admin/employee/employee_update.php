<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
if($employee_details<>"")
{
foreach ($employee_details as $emp_details) {
    $employee_id = $emp_details->employee_id;
    $employee_name = $emp_details->employee_name;
    $desig_db = $emp_details->designation;
    $org_id = $emp_details->org_id;
	$email = $emp_details->email;
	$organization_db = $emp_details->organization;
	$location = $emp_details->location;
	$themetic_team = $emp_details->themetic_team;
	$category_db = $emp_details->category;
	$mobile = $emp_details->mobile;
	$supervisor_db = $emp_details->supervisor;
}

}
?>
    <form id="update_employee_form">
			
            <input type="hidden" name="employee_id" id="employee_id" value="<?php echo $employee_id;?>" />	
            <div class="form-group">
              <label>Name of Employee <span class="text-danger">*</span></label>
              <input class="form-control" id="employee_name" name="employee_name" value="<?php echo $employee_name;?>">
                
            </div>
            
             <div class="form-group">
              <label>Designation<span class="text-danger">*</span></label>
              <select name="designation" id="designation" class="form-control">
              <option value="" selected>Select Designation</option>
              <?php 
              if($designation_list<>"")
              {
              foreach($designation_list as $dglist)
                {
                    $designation_id = $dglist->designation_id;
                    $designation = $dglist->designation;
					
					if($desig_db == $designation_id)
					{
					?>
                     <option value="<?php echo $designation_id;?>" selected="selected"><?php echo $designation;?></option>
                    <?php
					}else
					{
                    ?>
                     <option value="<?php echo $designation_id;?>"><?php echo $designation;?></option>
                    <?php 
					}
                }
              }
              ?>
              </select>      
            </div>
            
             <div class="form-group">
              <label>Category<span class="text-danger">*</span></label>
              <select name="category" id="category" class="form-control">
             
              <?php 
              if($category_list<>"")
              {
              foreach($category_list as $cglist)
                {
                    $category_id = $cglist->category_id;
                    $category = $cglist->category;
					
					if($category_db ==$category_id)
					{
					?>
                   <option value="<?php echo $category_id;?>" selected="selected"><?php echo $category;?></option> 
                    <?php
					}
					else
					{
                    ?>
                     <option value="<?php echo $category_id;?>"><?php echo $category;?></option>
                    <?php
					}
                }
              }
              ?>
              </select>     
            </div>
            
             <div class="form-group">
            <label>Thematic Team</label>
            <input class="form-control" id="thematic_team" name="thematic_team" value="<?php echo $themetic_team;?>">
            </div>
            
            <div class="form-group">
              <label>Organization<span class="text-danger">*</span></label>
              <select name="organization" id="organization" class="form-control">
              <option value="" selected>Select Organization</option>
              <?php 
              if($organization_list<>"")
              {
              foreach($organization_list as $oglist)
                {
                    $organization_id = $oglist->org_id;
                    $organization_name = $oglist->organization_name;
					if($organization_db == $organization_id)
					{
					?>
                    <option value="<?php echo $organization_id;?>" selected="selected"><?php echo $organization_name;?></option>
                    <?php
					}
					else
					{
                    ?>
                     <option value="<?php echo $organization_id;?>"><?php echo $organization_name;?></option>
                    <?php 
					}
                }
              }
              ?>
              </select>     
            </div>
            
            <div class="form-group">
            <label>Location</label>
            <input class="form-control" id="organization_location" name="organization_location" value="<?php echo $location;?>">
            </div>
            
            <div class="form-group">
            <label>Mobile No.</label>
            <input class="form-control" id="mobile_no" name="mobile_no" placeholder="Enter Mobile Number" value="<?php echo $mobile;?>">
            </div>
            
            <div class="form-group">
            <label>Email ID<span class="text-danger">*</span></label>
            <input class="form-control" id="email" name="email"  value="<?php echo $email;?>">
            </div>
            
            <div class="form-group">
            <label>Organization ID<span class="text-danger">*</span></label>
            <input class="form-control" id="org_id" name="org_id"  value="<?php echo $org_id;?>">
            </div>
            
           
            <div class="form-group">
            <label>Supervisor<span class="text-danger">*</span></label>
            <select class="form-control" name="supervisor" id="supervisor">
            <option value="" selected>Select Supervisor</option>
             <?php 
              if($supervisor_list<>"")
              {
              foreach($supervisor_list as $splist)
                {
                    $employee_id = $splist->employee_id;
                    $employee_name = $splist->employee_name;
					if($supervisor_db == $employee_id)
					{
					?>
                    <option value="<?php echo $employee_id;?>" selected="selected"><?php echo $employee_name;?></option>    
                    <?php
					}
					else
					{
                    ?>
                     <option value="<?php echo $employee_id;?>"><?php echo $employee_name;?></option>
                    <?php 
					}
                }
              }
              ?>
            </select>
            </div>
           
           

</form>

<!-- /.box-body -->
     