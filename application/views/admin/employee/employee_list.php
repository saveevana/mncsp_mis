			
				<div class="row">
				<div class="col-lg-12">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee Name</th>
                                        <th>Designation</th>
                                        <th>Category</th>
                                        <th>Thematic Team</th>
                                        <th>Organization</th>
                                        <th>Location</th>
                                        <th>Mobile #</th>
                                        <th>Email ID</th>
                                        <th>Org. ID</th>
                                        <th>Supervisor</th>
                                        <th>Create Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								  <?php
									$i = 1;
									if ($employee_list <> "") {
										foreach ($employee_list as $emp_list) {
											$employee_id = $emp_list->employee_id;
											$employee_name = $emp_list->employee_name;
											$designation = $emp_list->designation;
											$designation_name = $emp_list->desig_name;
											$org_id = $emp_list->org_id;
											$email = $emp_list->email;
											$organization = $emp_list->organization;
											$organization_name = $emp_list->organization_name;
											$location = $emp_list->location;
											$themetic_team = $emp_list->themetic_team;
											$category = $emp_list->category;
											$mobile = $emp_list->mobile;
											$supervisor = $emp_list->supervisor;
											$status = $emp_list->status;
											$create_date = $emp_list->create_date;
											?>
                                    <tr>
									<td><div align="center"><?php echo $i; ?></div></td>
									<td><?php echo $employee_name; ?></td>
									<td><?php echo $designation_name; ?></td>
									<td><?php echo $category; ?></td>
                                    <td><?php echo $themetic_team; ?></td>
                                    <td><?php echo $organization_name; ?></td>
                                    <td><?php echo $location; ?></td>
                                    <td><?php echo $mobile; ?></td>
                                    <td><?php echo $email; ?></td>
                                    <td><?php echo $org_id; ?></td>
                                    <td><?php get_user_supervisor($supervisor);?></td>
                                    <td><?php echo $create_date; ?></td>
                                    <td class="center">
									
                                    <a href="#" title="Edit Employee" class="btn btn-info btn-xs"
                                       data-toggle="modal" data-target="#modal-employee"
									   onclick="employee_edit('<?php echo $employee_id;?>');"><i
                                       class="fa fa-pencil fa-x"></i></a>
                                    &nbsp;
                                    <a href="#" title="Delete Employee" class="btn btn-danger btn-xs"
                                       onclick="return employee_delete('<?php echo $employee_id;?>');">
                                        <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
									
									</td>
                                    </tr>
                                  
                                   <?php 
								   $i++;
								   }
								   }
								   ?>
                                   
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                   
                </div>
				</div>
				
				<!-- Modal -->
                <div class="modal fade" id="modal-employee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Employee Details Update</h4>
                            </div>
                            <div class="modal-body">
                                <div id="employee_edit"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" onClick="return employee_update_submit();">Save changes</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
