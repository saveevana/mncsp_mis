			
				<div class="row">
				<div class="col-lg-10">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                           Designation Entry Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                   <form role="form" id="designation_form">
                                        <div class="form-group">
                                          <label>Designation<span class="text-danger">*</span></label>
                                            <input class="form-control" id="designation" name="designation" placeholder="Enter Designation">
                                            
                                        </div>
                                      
                                       
                                        <div align="right">
                                        <button type="button" class="btn btn-outline btn-default" onClick="return designation_create();">
                                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('SUBMIT'); ?>
                                        </button>
                                        </div>
                                    </form>
                                </div>
                              
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
				</div>

  