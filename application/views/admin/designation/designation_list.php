
				
				<div class="row">
				<div class="col-lg-12">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Designation Name</th>
                                        <th>Status</th>
                                        <th>Create Date</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
								  <?php
									$i = 1;
									if ($designation_list <> "") {
										foreach ($designation_list as $deg_list) {
											$designation_id = $deg_list->designation_id;
											$designation = $deg_list->designation;
											$status = $deg_list->status;
											$create_date = $deg_list->create_date;
											?>
                                    <tr>
									<td><div align="center"><?php echo $i; ?></div></td>
									<td><?php echo $designation; ?></td>
									<td><?php if($status == 1){ echo "Active"; }else { echo "Inactive"; }?></td>
									<td><?php echo $create_date; ?></td>
                                    <td class="center">
									
                                    <a href="#" title="Edit Designation" class="btn btn-info btn-xs"
                                       data-toggle="modal" data-target="#modal-designation"
									   onclick="return designation_edit('<?php echo $designation_id; ?>');"><i
                                       class="fa fa-pencil fa-x"></i></a>
                               
                                    &nbsp;
                                    <a href="#" title="Delete Organization" class="btn btn-danger btn-xs"
                                       onclick="return designation_delete('<?php echo $designation_id; ?>');">
                                        <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
                              
									</td>
                                    </tr>
                                  
                                   <?php 
								   $i++;
								   }
								   }
								   ?>
                                   
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                   
                </div>
				</div>
				
				<!-- Modal -->
                <div class="modal fade" id="modal-organization" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                <div id="designation_edit"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->


