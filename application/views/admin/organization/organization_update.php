<!-- Small boxes (Stat box) -->
<!-- /.row -->
<!-- Main row --><!-- /.row (main row) -->

<?php
foreach ($organization_details as $org_details) {
    $organization_id = $org_details->org_id;
    $organization_name = $org_details->organization_name;
    $organization_location = $org_details->organization_location;
    
}

?>
<form id="update_organization_form">
    <div class="box-body" id="">

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ORGANIZATION'); ?>
                    <span
                            class="text-danger">*</span></label>

                <input type="text" class="form-control" id="organization_name" name="organization_name"
                       value="<?php echo $organization_name; ?>">

            </div>

           
            <div class="form-group">
                <label for="exampleInputName" class="control-label"><?php echo load_message('ADDRESS'); ?></label>

                <input type="text" class="form-control number-only" maxlength="11" id="organization_phone" name="organization_phone"
                       value="<?php echo $organization_location; ?>">

            </div>
        </div>
       
    </div>
        <div align="center">
            <button type="submit" class="btn btn-primary btn-lg" onClick="handle_form('update_organization_form', organization, 'organization/organization_edit',<?php echo $organization_id; ?>);">
                <i class="fa fa-edit"></i> &nbsp; <?php echo load_message('UPDATE_ORGANIZATION'); ?>
            </button>
        </div>

</form>

<!-- /.box-body -->
     