
				
				<div class="row">
				<div class="col-lg-12">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                           
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Organization Name</th>
                                        <th>Address</th>
                                        <th>Create Date</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
								  <?php
									$i = 1;
									if ($organization_list <> "") {
										foreach ($organization_list as $org_list) {
											$organization_id = $org_list->org_id;
											$organization_name = $org_list->organization_name;
											$organization_location = $org_list->organization_location;
											$create_date = $org_list->create_date;
											?>
                                    <tr>
									<td><div align="center"><?php echo $i; ?></div></td>
									<td><?php echo $organization_name; ?></td>
									<td><?php echo $organization_location; ?></td>
									<td><?php echo $create_date; ?></td>
                                    <td class="center">
									<?php if (permission_check('organization/organization_edit')) { ?>
                                    <a href="#" title="Edit Organization" class="btn btn-info btn-xs"
                                       data-toggle="modal" data-target="#modal-organization"
									   onclick="organization_edit('<?php echo $organization_id; ?>');"><i
                                       class="fa fa-pencil fa-x"></i></a>
                                <?php }
                                if (permission_check('organization/organization_delete')) { ?>
                                    &nbsp;
                                    <a href="#" title="Delete Organization" class="btn btn-danger btn-xs"
                                       onclick="return organization_delete('<?php echo $organization_id; ?>');">
                                        <i class="fa fa-trash fa-x" aria-hidden="true"></i></a>
                                <?php } ?>
									
									</td>
                                    </tr>
                                  
                                   <?php 
								   $i++;
								   }
								   }
								   ?>
                                   
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                   
                </div>
				</div>
				
				<!-- Modal -->
                            <div class="modal fade" id="modal-organization" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="organization_edit"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->


