
				
				<div class="row">
				<div class="col-lg-10">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                           Organization Entry Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                   <form role="form" id="organization_form">
                                        <div class="form-group">
                                          <label>Category<span class="text-danger">*</span></label>
                                            <input class="form-control" id="organization_name" name="organization_name" placeholder="Enter Organization Name">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Location</label>
                                            <input class="form-control" id="organization_location" name="organization_location" placeholder="Enter Organization Location">
                                        </div>
                                       
                                        <div align="right">
                                        <button type="button" class="btn btn-outline btn-default" onClick="return organization_create();">
                                        <i class="fa fa-plus"></i> &nbsp; <?php echo load_message('SUBMIT'); ?>
                                        </button>
                                        </div>
                                    </form>
                                </div>
                              
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
				</div>

   