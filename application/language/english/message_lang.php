<?php

//USE load_message($key)
//System Related
$lang['SITE_TITLE'] = "Mamoni-MNCSP: MIS";
$lang['SITE_TYPE'] = "Management Information System";
$lang['LOGIN_TITLE'] = "Login";
$lang['MAINTAINENCE'] = 'Site Under Maintainence. You will face problem for using system.';
$lang['FORGOT_PASSWORD'] = 'Forgot Password?';
$lang['FORGOT_PASSWORD_MSG'] = 'Please contact with system administrator.';


//ORG
$lang['NIPORT'] = 'National Institute of Population Research and Training';
$lang['NIPORT_SHORT'] = 'NIPORT';
$lang['MINISTRY'] = "Ministry of Health and Family Welfare";
$lang['MINISTRY_SHORT'] = "MOH&FW";

//Labels
$lang['CATEGORY'] = 'Category';
$lang['SELECT_CATEGORY'] = 'Select Category';

//user .. profile
$lang['USER_UPDATE'] = 'User details is updated successfully!';
$lang['ERROR_USER_UPDATE'] = 'User details updated action is failed!';
$lang['USER_IMAGE_UPDATE'] = 'User image is uploaded';
$lang['USER_IMAGE_UPDATE_ERROR'] = 'Error in user image upload';


//employee
$lang['ADD_NEW_EMPLOYEE'] = 'Employee Registration';


//privilage
$lang['PRIVILEGE_CREATED'] = 'Privilege is created';
$lang['PRIVILEGE_CREATE_FAILED'] = 'Failed to create privilege';

//category
$lang['CATEGORY_CREATED'] = 'Category is created';
$lang['CATEGORY_CREATE_FAILED'] = 'Failed to create category';
$lang['CATEGORY_UPDATED'] = 'Category is updated';

//office
$lang['OFFICE_CREATED'] = 'New office added';
$lang['OFFICE_UPDATED'] = 'Office information updated';
$lang['ROOM'] = 'Room Name';


//Quick report
$lang['QUICK_REPORT_TITLE'] ="Institute wise reporting";
$lang['NIPORT_INSTITUTES']= "Institutes";

$lang['EXCEL_IMPORT']="Import Asset";

$lang['YEAR'] = 'Year';
$lang['MONTH'] = 'Month';
$lang['DAY'] = 'day';

$lang['INST_ASSET_VALUE']='Institute Wise Asset Value';

$lang['DATABASE_BACKUP'] = 'Backup Database';
$lang['EXPORT_SUCCESS'] = 'Database is exported successfully!';

$lang['SUBMIT'] = 'Submit';
$lang['EMPLOYEE_LIST'] = 'Employee List';
$lang['ADD_NEW_DESIGNATION']='Designation Create'
?>
